<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

# About

Flock is browser compatible protocol that allow nodes to exchange resources through peer-to-peer connections. 
This section will describe how to access the flock through aflap, an application that uses flock standards to share files with other peers.

For advanced documentation about the flock protocols, you can read the [project Readme](../../../../Readme.md), 
and about the various layers of the flock (
[transport abstraction](../../../Flock/Layers/TransportAbstraction/Readme.md),
[topology](../../../Flock/Layers/Topology/Readme.md),
[application](../../../Flock/Layers/Application/Readme.md),
[end service](../../../Flock/Layers/EndService/Readme.md),
)


# Connecting to a live flock application

A live example of the aflap application is available at this address : 
[www.flockproject.com](http://www.flockproject.com)

To connect between peers, it relies on webrtc. In case of problems, make sure webrtc is working on your browser.
Older versions of IE will not work : utilization of firefox/chrome recommended.

# Connecting without using a websites.

The point of flock is to get rid of intermediaries that might spy on your navigation habits. 
The only role of the precedent website example is to deliver the static content that will enable the peer connection and resource sharing.

To access the same flock example without an intermediate website :
- download the "dist" folder at the base of thi project
- open "dist/aflap/index.html" in your own browser.

# Using Aflap with other parameters

You can play with parameters in the dist/aflap/resources/js/aflapconf.js file to personalize the way aflap access the flock.
