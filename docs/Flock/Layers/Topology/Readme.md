<!---
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
# Description

The topology layer purpose is to contact other nodes, get their informations, and keep alive enough connections to ensure the consistency of the whole network.
The protocol used for the topology layer is "flock". Implementation of the standard might build a component with the same name.

The default port (to use in [the message dictionnary of the transportAbstraction layer](../TransportAbstraction/Readme.md) ) for the flock protocol is 1.

# Flock keys

Keys identify resources in a flock architecture. Creation and utilisation of keys depends on the [application layer](../Application/Readme.md).
In the topology layer, keys are defined by the key dictionnary with the following EE :
- { n : "prefix", t : "hex", c : 2, dd : ""}
- { n : "common", t : "int", c : 3}
- { n : "hiddenLength", t : "int", c : 4, dd : 0}
- { n : "span", t : "int", c : 5, dd : 0}

**prefix** is the whole key if "span" is null, or the beginning of a key range if "span" is not null.

**common** : some flock methods require to send keys. the "common" entry allow you to only send the diverging suffix of keys. It specifies the byte length that are common with the requested key

**hiddenLength** : in case a key is too long and its whole value is not needed by the requester, this attribute allow to truncate a key by specifying how many bytes are not returned.

**span** : it defines a key span rather than a key. a key span is the whole set of key that begins with the prefix and that are up to "span" bytes longer.



When an application needs to indicates that the node is responsible / can provide a given key or key span, it should be done via a "addKey" method of the flock component.

# Protocol

The flock component should be able to listen to remote messages and address appropriate answer. 

### Node dictionnary

In flock messages, nodes are represented by the following EE dictionnary :
- {n:"id", t:"int", c:2};
- {n:"keys", t:"arr", tpl : TEMPLATE_KEY, c:3}
- {n:"canals", t:"arr", tpl : TEMPLATE_CANAL, c:4}

**id** : is a random id (advised : 6 bytes long, but should manage longer ids), that is affected by the flock component to itself upon start. It serves to identfy it. Assume that randomness make it relatively unique among other nodes

**keys** : list of keys managed by the node

**canals** : list of canals by wich the node is accessible

### Request dictionnay

flock nodes exchange messages among themselves, defined as request dictionnary EE : 
- { n : "method", t : "int", c : 2, r : true}
- { n : "requestId", t : "hex", c : 3}
- { n : "k", t : "int", c : 4, d : 1}
- { n : "precision", t : "int", c : 5, d : 1}
- { n : "prefix", t : "hex", c : 6, d : new Uint8Array() }
- { n : "nodes", t : "arr", tpl : this.TEMPLATE_NODE, c : 7, d : []}
- { n : "flags", t : "int", c : 8, d : 0}

		
**method** defines the type of message. It can take one of the following values :

| name | code | description |
| ---- | ---- | ----------- |
| getNode | 0 | ask for connected peers of a node. Select nodes closest to a given key |
| giveNode | 1 | answer of a getNode request |
| getInfo | 2 | ask personal informations about a node like keys, id and canals |
| giveInfo | 3 | answer for a getInfo request |
		
**requestId** : a random 4-Byte hexadecimal generated during getNode and getInfo packets, and used again in answer (giveNode, giveInfo packets), to match an answer with the initial requests.

**k** : number of expected node results when sending a getNode packet. Expect valid nodes (ie alive) as an answer.

**precision** : maximum length of prefix attribute in nodes given in giveNodes packets. the rest of the key is notified as the hiddenLength in the key dictionnary.

**prefix** : target prefix of the giveNode request. Answered nodes should be as close as possible, with distance between prefix calculated with XOR metrics.

**nodes** : array of nodes returned in giveNode. In giveInfo packet, this array contains a single node object with the requested data

**flags** : list of data asked in a giveInfo request. It is the sum of several flags (set to ) if the information is requested :

| bit value | requested info | 
| --------- | -------------- |
| 128       | keys           | 
| 64        | canals         |
| 32        | id             |


# Topology 

Each flock node is manage one or several resources, identified by their keys. 

To access a resource, a node need to contact another node who manage a keyspan containing the right key.
To do so, it must sends getNode requests, starting to the closest nodes, until it reaches the right node.

Distance between keys are calculated with the same [XOR metrics that Kademlia uses](https://en.wikipedia.org/wiki/Kademlia). To calculate a distance between two keys or keyspan :
- if a key prefix is longer than the other, extend the smaller by appending 0 to it
- apply XOR to the two prefixes

The more similarity between prefixes, the smaller the distance. First bits of a key have then a highest impact on distance between keys.

In order to define similarity between two keys, we introduce the concept of bucket. 
Consider two keys K1 and K2, D their XOR distance, i a positive integer. 
K2 is in the i-bucket of K1 if the bit of D in position i is equal to 1 and all bits in position lower than i are equal to 0.

With this definition, we can define a condition to ensure the flock network consistency. 
For each managed key, a node must keep connections alive for at least one node per bucket.
A cron task should be launched by the flock component to check whether some buckets are not filled.
 


