let aflapconf = {
    // most traffic goes through webrtc : in order for signaling to occur, we need to define a swan server that will make our local node accessible from other peers
    swansServerCanals : [{"type":2,"address":"ws://flockproject.com:10000"},{"type":1,"address":"http://flockproject.com:10001"}],

    // peers that will be reached on start by the flock. Ideally, specify one or several squawkers to access and be accessible quickly
    nodes : [{ canals : [{"type":2,"address":"ws://flockproject.com:10000"},{"type":1,"address":"http://flockproject.com:10001"}] }],

    // stun servers for webrtc communication : specify two for optimal efficiency
    stunServers : [
        {urls:'stun:stun.l.google.com:19302'},
        {urls:'stun:stun1.l.google.com:19302'},
    ],

    // period of the flock cron :
    // the flock cron check the peers, ensure they are connected, clear failed nodes, provide enough peer for the topology layer...
    cronInterval : 10000,

    // when a node doesn't send topology message for too long, consider this node dead, close the connection and remove it from the pool of nodes
    nodeExpireTime : 20000,

    // maximum  number of connections that can be opened : this limit is due to browser webrtc limits : you can increase in non-browser environments
    maxInterfacedNodes : 255,


    // enable the creation of connection to peers that are accessible through a webrtc interface
    withWebrtcSpawn : true,
    // create a spawner to make the node accessible on demand through webrtc interface
    withWebrtcSpawner : true,

    // enable the creation of connection to peers that are accessible through a http interface
    withHttpSpawn : true,
    // create a spawner to make the node accessible on demand through http interface
    withHttpSpawner : false,

    // enable the creation of connection to peers that are accessible through a swans interface
    withSwansSpawn : true,
    // create a spawner to make the node accessible on demand through swans interface
    withSwansSpawner : false,

    // enable the creation of connection to peers that are accessible through a websocket interface
    withWebsocketSpawn : true,
    // create a spawner to make the node accessible on demand through websocket interface
    withWebsocketSpawner : false,
}