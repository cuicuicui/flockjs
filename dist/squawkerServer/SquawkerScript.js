/***
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 **/

const args = require('minimist')(process.argv.slice(2))

// simple script to create a squawker that enable peers to create swans connections and advertise existing nodes based on their keys
// NB : IF IT IS STARTED FROM A DOCKER CONTAINER, PORTS OF THE HOST MUST BE THE SAME THAN THOSE OF THE DOCKER CONTAINER FOR THE MOMENT

global["websocketPort"] = 10000;
global["httpPort"]  = 10001;
global["serverAddress"]  = "127.0.0.1";
console.log(args);

for(let index in args){
    if(index === "_") continue;
    console.log("received external param "+index+" with value "+args[index]+" replacing default value "+global[index]);
    global[index] = args[index];
};


// tested modules
var flocklibs = require("./flock-nodejs.js");
Object.assign(global, flocklibs);

// Modify the following data if needed
let params = {
    // address for websockets/http servers
    serverAddress : serverAddress,
    // port to be used for new messaging through websockets
    websocketPort : websocketPort,
    // port that will be used to receive http requests
    httpPort : httpPort,
    // authorize connections through websockets
    withWebsocketSpawn : true,
    withWebsocketSpawner : true,
    // authorize connections through webrtc
    withWebrtcSpawner : false,
    withWebrtcSpawn : false,
    // authorize connections through http
    withHttpSpawner : true,
    withWebsocketSpawner : true,
    //authorize swans connections
    withSwansSpawn : true,
    withSwansSpawner : true,
    nodeExpireTime : 10000,
    cronInterval : 10000 // disable cron by default
}

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
});


//create the squawker
let squawker = new Squawker(params);

// once the squawker is ready, print its canals
squawker.isReady().then(function(){
    console.log("Squawker started with canals ::::::: ")
    let canals = squawker.getCanals();
    console.log( JSON.stringify(canals) );
}, 5000)


// prevent process from closing
setInterval(function(){}, 1000000)